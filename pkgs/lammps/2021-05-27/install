#!/bin/bash

prefix=$1
version=$2

module load cmake

name="lammps"
version=$(date -d "$version" +%d%b%Y)
md5sum="efa5665e2015ee46e23301d9ffca8b3f"
archive="patch_$version.tar.gz"
url="https://github.com/lammps/lammps/archive/$archive"

wget "$url"
md5sum --check <<<"$md5sum $archive"
tar -xaf "$archive"
cd "$name-patch_$version"

opts=(
    -DBUILD_MPI=yes
    -DBUILD_OMP=yes
    -DBUILD_SHARED_LIBS=yes
    -DLAMMPS_EXCEPTIONS=yes

    # install most packages
    -C ../cmake/presets/most.cmake
    -DPKG_PYTHON=no
)

if module is-loaded intel ; then
    opts+=( -DPKG_USER-INTEL=yes -DFFTW=MKL )
else
    module-load-and-depend fftw/3
    opts+=( -DFFTW=FFTW3 )
fi

mkdir build
pushd build
cmake ../cmake \
    -DCMAKE_INSTALL_PREFIX="$prefix" \
    "${opts[@]}"
make -j "$(nproc)"
make install
popd

find examples -xtype l -delete # delete broken symlinks
rsync --copy-links --recursive examples "$prefix/share/lammps"
rsync --recursive python "$prefix"

