#!/bin/bash

prefix=$1
version="2021-09-29"

module load cmake

name="lammps"
version=$(date -d "$version" +%d%b%Y)
md5sum="b09c5c7e335590e02e71798825eeb168"
archive="patch_$version.tar.gz"
url="https://github.com/lammps/lammps/archive/$archive"

wget "$url"
md5sum --check <<<"$md5sum $archive"
tar -xaf "$archive"
cd "$name-patch_$version"

opts=(
    -DBUILD_MPI=no
    -DBUILD_OMP=yes
    -DBUILD_SHARED_LIBS=yes
    -DLAMMPS_EXCEPTIONS=yes

    # install most packages
    -C ../cmake/presets/most.cmake

    -DPKG_PYTHON=no
)

# Mist only
if module is-loaded cuda ; then
    opts+=( -DPKG_GPU=on -DGPU_API=cuda )
fi

if module is-loaded intel ; then
    opts+=( -D PKG_INTEL=yes -DPKG_USER-INTEL=yes -DFFTW=MKL )
else
    module-load-and-depend fftw/3
    opts+=( -DFFTW=FFTW3 )
fi

mkdir build
pushd build
cmake ../cmake \
    -DCMAKE_INSTALL_PREFIX="$prefix" \
    "${opts[@]}"
make -j "$(nproc)"
make install
popd

find examples -xtype l -delete # delete broken symlinks
rsync --copy-links --recursive examples "$prefix/share/lammps"
rsync --recursive python "$prefix"

