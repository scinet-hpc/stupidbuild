#!/bin/bash

if [ "${CC-}" ] ; then
    if grep -qi intel /proc/cpuinfo; then
        if module load mkl; then
            module unload mkl
            module-load-and-depend mkl
            BLASLINKLINE="-Wl,-rpath,$MKLROOT/lib/intel64 -Wl,-rpath,$MKLROOT/../compiler/lib/intel64 -Wl,--no-as-needed -Wl,--start-group -lmkl_rt -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -Wl,--end-group -lgomp -lpthread -lm -ldl"
            export MKL_INTERFACE_LAYER=GNU
        fi
    fi
else
    if grep -qi intel /proc/cpuinfo; then
        if module load gcc mkl; then
            BLASLINKLINE="-Wl,-rpath,$MKLROOT/lib/intel64 -Wl,-rpath,$MKLROOT/../compiler/lib/intel64 -Wl,--no-as-needed -Wl,--start-group -lmkl_rt -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -Wl,--end-group -lgomp -lpthread -lm -ldl"
            export MKL_INTERFACE_LAYER=GNU
            export LIBRARY_PATH="$LIBRARY_PATH:$MKLROOT/lib/intel64:$MKLROOT/../compiler/lib/intel64"
        fi
        module unload gcc mkl
    fi
    module load gcc/.core
fi
   
module load autotools

if [ "$(uname -m)" = ppc64le ]; then
    export CFLAGS="-O2 -mtune=native"
    module load ibm-java ||:
else
    export CFLAGS="-O2 -ftree-vectorize -march=native"
    module load java ||:
fi

GCCBASE=$(dirname $(dirname `which gcc`))
export LDFLAGS="-fopenmp -Wl,-rpath,$GCCBASE/lib64"
export CXXFLAGS="$CFLAGS"
export FFLAGS="$CFLAGS"

unset version
unset archive
unset md5sum
unset url
unset prefix
unset lib
unset cfg
unset cfgopt
unset makeopt
unset beforecfg

declare -A version
declare -A archive
declare -A md5sum
declare -A url
declare -A prefix
declare -A lib
declare -A cfg
declare -A cfgopt
declare -A makeopt
declare -A beforecfg

pkg="R"
version[$pkg]="4.3.3"
archive[$pkg]="$pkg-${version[$pkg]}.tar.gz"
md5sum[$pkg]="4de100b35e3614c19df5e95e483cc3c"3
url[$pkg]="https://cran.r-project.org/src/base/$pkg-${version[$pkg]%%.*}/${archive[$pkg]}"
cfgopt[$pkg]="--enable-rpath --with-lapack --enable-memory-profiling --enable-R-profiling --enable-R-shlib --enable-shared  --enable-largefile --enable-nls --with-cairo --with-libpng --with-jpeglib --with-libtiff --with-pcre2"
prefix[$pkg]="$1"
beforecfg[$pkg]='eval export CPATH=$CPATH:${prefix[pcre2]}/include'

# install bz2
#
wget https://github.com/nemequ/bzip2/releases/download/v1.0.6/bzip2-1.0.6.tar.gz
tar xaf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make -f Makefile-libbz2_so CC=core-gcc CFLAGS="-fPIC -O3 -march=native" LDFLAGS="-Wl,-rpath=${prefix[R]}/bz2/lib" -j $(nproc)
make
make install PREFIX=${prefix[R]}/bz2 # may be too much
ln -s libbz2.so.1.0 libbz2.so
cp -vd libbz2.so.1.0 libbz2.so.1.0.6 libbz2.so "${prefix[R]}/bz2/lib"
cd ..
export LIBRARY_PATH="$LIBRARY_PATH:${prefix[R]}/bz2/lib"
export CPATH="$LIBRARY_PATH:${prefix[R]}/bz2/include"

pkg="tiff"
version[$pkg]="4.6.0"
archive[$pkg]="$pkg-${version[$pkg]}.tar.gz"
md5sum[$pkg]="fc7d49a9348b890b29f91a4ecadd5b49"
url[$pkg]="http://download.osgeo.org/libtiff/${archive[tiff]}"
prefix[$pkg]="${prefix[R]}/requirements/$pkg"
cfgopt[$pkg]="--enable-rpath"
lib[$pkg]="${prefix[$pkg]}/lib"
cfg[$pkg]="${prefix[$pkg]}/lib/pkgconfig"


pkg="freetype"
version[$pkg]="2.13.2"
archive[$pkg]="freetype-2.13.2.tar.xz"
md5sum[$pkg]="1f625f0a913c449551b1e3790a1817d7"
url[$pkg]="https://gigenet.dl.sourceforge.net/project/freetype/freetype2/${version[freetype]}/${archive[freetype]}"
prefix[$pkg]="${prefix[R]}/requirements/$pkg"
cfgopt[$pkg]=""
lib[$pkg]="${prefix[$pkg]}/lib64"
cfg[$pkg]="${prefix[$pkg]}/lib64/pkgconfig"
beforecfg[$pkg]="STOP_THEN_INSTEAD_USE_MESON_AND_NINJA"

pkg="pixman"
version[$pkg]="0.43.4"
archive[$pkg]="$pkg-${version[$pkg]}.tar.gz"
md5sum[$pkg]="21b18058dea39ad48f32d3199b8ffe40"
url[$pkg]="https://cairographics.org/releases/${archive[$pkg]}"
prefix[$pkg]="${prefix[R]}/requirements/$pkg"
cfgopt[$pkg]=""
lib[$pkg]="${prefix[$pkg]}/lib64"
cfg[$pkg]="${prefix[$pkg]}/lib64/pkgconfig"
beforecfg[$pkg]="STOP_THEN_INSTEAD_USE_MESON_AND_NINJA"

pkg="cairo"
version[$pkg]="1.18.0"
archive[$pkg]="$pkg-${version[$pkg]}.tar.xz"
md5sum[$pkg]="3f0685fbadc530606f965b9645bb51d9"
url[$pkg]="https://cairographics.org/releases/${archive[$pkg]}"
prefix[$pkg]="${prefix[R]}/requirements/$pkg"
lib[$pkg]="${prefix[$pkg]}/lib"
cfg[$pkg]="${prefix[$pkg]}/lib/pkgconfig"
beforecfg[$pkg]="STOP_THEN_INSTEAD_USE_MESON_AND_NINJA"

pkg="pcre2"
version[$pkg]="10.35"
archive[$pkg]="$pkg-${version[$pkg]}.tar.gz"
md5sum[$pkg]="626ae9ad9a1d4a09ef4221c885574ff5"
url[$pkg]="https://ftp.exim.org/pub/pcre/${archive[$pkg]}"
prefix[$pkg]="${prefix[R]}/requirements/$pkg"
cfgopt[$pkg]="" #--enable-unicode --enable-jit --enable-pcre2-16 --enable-pcre2-32 --enable-pcre2grep-libz --enable-pcre2grep-libbz2 --enable-pcre2test-libreadline"
lib[$pkg]="${prefix[$pkg]}/lib"
cfg[$pkg]="${prefix[$pkg]}/lib/pkgconfig"

pkg="libxml2"
version[$pkg]="2.12.6"
archive[$pkg]="$pkg-${version[$pkg]}.tar.xz"
md5sum[$pkg]="37fab9ace78d8a085af81dc9a19f36d9"
url[$pkg]="https://download.gnome.org/sources/libxml2/${version[$pkg]%.*}/${archive[$pkg]}"
prefix[$pkg]="${prefix[R]}/requirements/$pkg"
cfgopt[$pkg]="--without-python"
lib[$pkg]="${prefix[$pkg]}/lib"
cfg[$pkg]="${prefix[$pkg]}/lib/pkgconfig"

export CPATH="$CPATH:${prefix[$pkg]}/include"

pkgs_install_order="libxml2 tiff freetype pixman cairo pcre2 R"

if ! [ -v BLASLINKLINE ]; then
    pkg="OpenBLAS"
    if module load openblas; then
        BLASLINKLINE=$(pkg-config --libs openblas)
        lib[$pkg]=$(pkg-config --variable=libdir openblas)
    else
        pkgs_install_order="$pkg $pkgs_install_order"
        version[$pkg]="0.3.9"
        archive[$pkg]="${pkg}-${version[$pkg]}.tar.gz"
        md5sum[$pkg]="28cc19a6acbf636f5aab5f10b9a0dfe1"
        url[$pkg]="https://github.com/xianyi/${pkg}/archive/v${version[$pkg]}.tar.gz"
        makeopt[$pkg]="USE_LOCKING=1 USE_OPENMP=0 USE_THREAD=0"
        #to make a threaded version, do:
        #makeopt[$pkg]="USE_OPENMP=0 USE_THREAD=1"
        prefix[$pkg]="${prefix[R]}/requirements/$pkg"
        lib[$pkg]="${prefix[$pkg]}/lib"
        cfg[$pkg]="${prefix[$pkg]}/lib/pkgconfig"
        beforecfg[$pkg]="touch configure"
        BLASLINKLINE="-L${prefix[$pkg]}/lib -lopenblas"
        CFLAGS="-I${prefix[$pkg]}/include $CFLAGS"
        CXXFLAGS="-I${prefix[$pkg]}/include $CXXFLAGS"
    fi
fi

builddir="$PWD"

for pkg in ${!url[@]}
do
    wget --quiet "${url[$pkg]}" -O "${archive[$pkg]}"
    md5sum --check <<<"${md5sum[$pkg]} ${archive[$pkg]}"
    tar -xaf "${archive[$pkg]}"
    mkdir -p "${prefix[$pkg]}"
done

for pkglib in ${lib[@]}; do
    export LDFLAGS="$LDFLAGS -Wl,-rpath,${pkglib}"
done

for pkgcfg in ${cfg[@]}; do
    if [ "${PKG_CONFIG_PATH-}" ]; then
        export PKG_CONFIG_PATH="$pkgcfg:$PKG_CONFIG_PATH"
    else
        export PKG_CONFIG_PATH="$pkgcfg"
    fi
done

for pkg in $pkgs_install_order
do
    cd "$builddir/$pkg-${version[$pkg]}"
    if [ "${beforecfg[$pkg]-}" == "STOP_THEN_INSTEAD_USE_MESON_AND_NINJA" ]; then
        module load meson ninja
        mkdir buildsubdir
        export MESON_PRINT_LIB_DIRS=1
        meson setup buildsubdir --prefix="${prefix[$pkg]}"
        ninja -C buildsubdir
        ninja -C buildsubdir install       
    else
        ${beforecfg[$pkg]-}
        chmod +x ./configure
        ./configure --prefix="${prefix[$pkg]}" "--with-blas=${BLASLINKLINE}" ${cfgopt[$pkg]-} 
        make -j $(nproc) ${makeopt[$pkg]-}
        make install PREFIX="${prefix[$pkg]}"
    fi
done

if which java &> /dev/null; then
    ${prefix[R]}/bin/R CMD javareconf
fi

retcdir=$(find ${prefix[R]} -type d -name etc)
cat > ${retcdir}/Rprofile.site <<EOF
options(bitmapType='cairo')
options(repos=structure(c(CRAN='https://cran.r-project.org')))
EOF

#
# Add common packages
#

r4install()
{
    PKGLST="'$1'"
    for pkg in ${*:2}; do
        PKGLST="$PKGLST,'$pkg'"
    done
    ${prefix[R]}/bin/R -q --no-save <<<"install.packages(c($PKGLST));q()"
}

r4install Matrix SuppDists
r4install acepack afex ape bdsmatrix doMC doMC ADGofTest AER BradleyTerry2
r4install brew brglm coda coin ctv Cairo # copula gsl DiagrammR
r4install doParallel DT dygraphs e1071 estimability feather forecast Formula fracdiff functional
r4install gdata httpuv xtable sourcetools fastmap shiny ggvis git2r gplots gridBase gridExtra gsw gtools Hmisc
r4install hms igraph irlba jpeg kohonen Lahman latticeExtra leaflet lintr lmtest
r4install emmeans lsmeans lubridate manipulate mapproj maptools microbenchmark mlmRev modeltools multcomp
r4install mvtnorm networkD3 NLP NMF numDeriv nycflights13 oce packrat perm pkgmaker
r4install PKI plm png praise pROC profileModel proto pryr pspline
r4install quadprog raster RcppArmadillo RCurl registry reshape rex rgl rJava
r4install rjson RJSONIO rngtools ROCR roxygen2 Rserve RSQLite rstudioapi rversions
r4install sandwich seacarb slam sp stabledist stringdist testit testthat TH.data
r4install threejs timeDate tm tseries VGAM visNetwork whisker withr xml2
r4install caret chron formatR glmnet hexbin randomForest rbokeh rmarkdown gistr

# add tidyverse and tidytext
r4install tidyverse tidytext
