--- build_rules.mk.orig	2024-01-26 09:50:53.196107000 -0500
+++ build_rules.mk	2024-01-26 09:50:30.313068000 -0500
@@ -183,10 +183,10 @@
 ESMF_F90LINKOPTS          += -m32
 endif
 ifeq ($(ESMF_ABISTRING),x86_64_small)
-ESMF_CXXCOMPILEOPTS       += -m64 -mcmodel=small
-ESMF_CXXLINKOPTS          += -m64 -mcmodel=small
-ESMF_F90COMPILEOPTS       += -m64 -mcmodel=small
-ESMF_F90LINKOPTS          += -m64 -mcmodel=small
+ESMF_CXXCOMPILEOPTS       += -m64 -mcmodel=small -fp-model precise
+ESMF_CXXLINKOPTS          += -m64 -mcmodel=small -fp-model precise
+ESMF_F90COMPILEOPTS       += -m64 -mcmodel=small -fp-model precise
+ESMF_F90LINKOPTS          += -m64 -mcmodel=small -fp-model precise
 endif
 ifeq ($(ESMF_ABISTRING),x86_64_medium)
 ESMF_CXXCOMPILEOPTS       += -m64 -mcmodel=medium
