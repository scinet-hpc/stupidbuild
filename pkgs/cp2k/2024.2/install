#!/bin/bash

prefix="$1"

[[ -v CC ]]

arch=$(uname -m)

case $arch in
	x86_64)  

		module load cmake/3.27.8
		module load openblas/0.3.15
		module load scalapack/2.1.0
		module load fftw/3.3.10
		module load boost/1.78.0
		module load gmp/6.2.1
		module load libint-cp2k/2.6
		module load libxc/6.2.2 
		module load gsl/2.7
		module load hdf5/1.10.9

		flags="" ;;
	ppc64le) 

		module load anaconda3/2021.05
		module load autotools
		module load cuda/11.0.3
		module load gcc/9.4.0
		module load cmake/3.19.8
		module load openblas/0.3.15
		module load scalapack/2.1.0
		module load fftw/3.3.9
		module load boost/1.76.0
		module load gmp/6.2.1
		module load libint-cp2k/2.6
		module load libxc/5.1.5
		module load gsl/2.7
		module load hdf5/1.10.7

		flags="--gpu-ver=V100 --enable-cuda" ;;

esac

#
# Install CP2K
#
version="2024.2"
cd $prefix
git clone https://github.com/cp2k/cp2k.git
cd cp2k
git checkout support/v$version
git submodule update --init --recursive

set +f

cd ..
mv cp2k/* .
rm -rf cp2k

cd tools/toolchain

# Look for HDF5 and SPLA libraries in correct location
sed -i 's/pkg_install_dir=$(h5cc.*/pkg_install_dir=$MODULE_HDF5_PREFIX/g' scripts/stage7/install_hdf5.sh
sed -i 's/include\/spla/include/g' scripts/stage8/install_spla.sh

case $arch in
    x86_64)  flags="" ;;
    ppc64le) flags="--gpu-ver=V100 --enable-cuda" ;;
esac

./install_cp2k_toolchain.sh $flags \
	--with-openmpi=system 	\
	--with-cmake=system 	\
	--with-openblas=system 	\
	--with-fftw=system 	\
	--with-libxc=system 	\
	--with-libint=system	\
	--with-hdf5=system 	\
	--with-scalapack=system \
	--with-gsl=system

cd ../..

cp ./tools/toolchain/install/arch/* ./arch/

# Look for libint header/mod files in correct location
sed -i "14s,$, -I$MODULE_LIBINT_CP2K_PREFIX/include," ./arch/*

# Use MPI compiler to compiler binaries when linking in parallel HDF5 library
sed -i 's/FC .*/FC = $(MPIFC)/g' arch/local.ssmp
sed -i 's/LD .*/LD = $(MPIFC)/g' arch/local.ssmp
sed -i 's/FC .*/FC = $(MPIFC)/g' arch/local.sdbg
sed -i 's/LD .*/LD = $(MPIFC)/g' arch/local.sdbg

set +u
source tools/toolchain/install/setup
set -u

make -j$(nproc) ARCH=local VERSION="sdbg ssmp pdbg psmp"

case $arch in
	ppc64le) make -j$(nproc) ARCH=local_cuda VERSION="sdbg ssmp pdbg psmp" ;;
esac

# Create symbolic link "cp2k" to the MPI + OpenMP version "cp2k.psmp"
ln -sT exe/local/cp2k.psmp exe/local/cp2k
