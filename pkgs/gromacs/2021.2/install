#!/bin/bash

prefix=$1
version=$2

module load cmake

version="2021.2"
archive="gromacs-$version.tar.gz"
url="http://ftp.gromacs.org/pub/gromacs/$archive"
md5sum="a58ff0662eae5818bddfa740a6c35b2e"

wget "$url"
md5sum --check <<<"$md5sum $archive"
tar -xaf "$archive"
cd "gromacs-$version"

cmake_opts=(
    -DCMAKE_INSTALL_PREFIX="$prefix"
    -DGMX_BUILD_OWN_FFTW=ON
    -DGMX_OPENMP=ON
)

if $CC -dM -E -x c /dev/null | grep -q __clang__; then
    cmake_opts+=(
        -DGMX_GPLUSPLUS_PATH="$(which gcc 2>/dev/null || which core-g++)"
    )
fi

if [[ $CC =~ ^mpi ]]; then
    cmake_opts+=( -DGMX_MPI=ON )
else
    # Despite the name, thread-MPI doesn't use MPI.
    # It's an internal implementation of MPI using pthreads.
    cmake_opts+=( -DGMX_THREAD_MPI=ON )
fi

if module is-loaded cuda; then
    # e.g., GMX_CUDA_TARGET_SM="70;75"
    # shellcheck disable=SC2005
    sm=$(echo $(deviceQuery | gawk '/Capability/ { print $6 }' | tr -d '.' | sort -u) | tr ' ' ';')
    cmake_opts+=(
        -DCUDA_TOOLKIT_ROOT_DIR="$MODULE_CUDA_PREFIX"
        -DGMX_CUDA_TARGET_SM="$sm"
        -DGMX_CUDA_TARGET_COMPUTE="$sm"
        -DGMX_GPU=CUDA
    )
elif module is-loaded rocm; then
    cmake_opts+=( -DGMX_GPU=OpenCL )
fi

mkdir build
cd build
cmake .. "${cmake_opts[@]}"

make -j "$(nproc)"
make -j "$(nproc)" check
make install

