#!/bin/bash

prefix=$1
module load gcc/.core

# download gcc
gcc="gcc-8.5.0"
sha512sum="92f599680e6b7fbce88bcdda810f468777d541e5fddfbb287f7977d51093de2a5178bd0e6a08dfe37090ea10a0508a43ccd00220041abbbec33f1179bfc174d8"
archive="$gcc.tar.xz"
url="https://gcc.gnu.org/pub/gcc/releases/$gcc/$archive"
wget "$url"
sha512sum --check <<<"$sha512sum $archive"
tar -xaf "$archive"

# download prerequisites
pushd "$gcc"
./contrib/download_prerequisites
popd

configure_opts=(
    --prefix="$prefix"
    --disable-multilib
    --enable-languages="c,c++,fortran,lto"
)

#
# CUDA/ROCm support
#
# https://gcc.gnu.org/wiki/Offloading#How_to_try_offloading_enabled_GCC
# https://github.com/tschwinge/gcc-playground/tree/big-offload/master
# https://kristerw.blogspot.com/2017/04/building-gcc-with-support-for-nvidia.html
# https://gcc.gnu.org/wiki/Offloading#For_AMD_GCN:
#

if module is-loaded cuda; then

    offload_target=nvptx-none
    configure_opts+=(
        --with-cuda-driver="$MODULE_CUDA_PREFIX"
    )

    # change default from sm_30 to sm_35; fixed in gcc 11
    pushd "$gcc"
    sed -i 's/sm_30/sm_35/' gcc/config/nvptx/nvptx.c
    popd

    # dowload & compile nvptx-tools
    git clone -n https://github.com/MentorEmbedded/nvptx-tools
    pushd nvptx-tools
    git checkout d0524fbdc86dfca068db5a21cc78ac255b335be5
    ./configure --prefix="$prefix" \
        --with-cuda-driver="$MODULE_CUDA_PREFIX"
    make -j$(nproc)
    make install
    popd

elif module is-loaded rocm; then

    offload_target=amdgcn-amdhsa

    # workaround for llvm-mc bug, fixed in llvm 12
    # https://gcc.gnu.org/bugzilla/show_bug.cgi?id=97827
    pushd "$gcc"
    wget https://gcc.gnu.org/pipermail/gcc-patches/attachments/20201118/b8e47a8d/attachment.bin
    git apply attachment.bin
    popd

    # gcc uses old llvm 9 options
    # https://reviews.llvm.org/D89076#inline-827796
    sed -i 's/-mattr=-code-object-v3/--amdhsa-code-object-version=2/' "$gcc/gcc/config/gcn/gcn-hsa.h"

    # gcc assembler doesn't handle amd gcn yet, so use llvm
    mkdir "$prefix/$offload_target"
    mkdir "$prefix/$offload_target/bin"
    src="$MODULE_ROCM_PREFIX/llvm/bin"
    dst="$prefix/$offload_target/bin"
    ln -s "$src/llvm-ar"    "$dst/ar"
    ln -s "$src/llvm-ar"    "$dst/ranlib"
    ln -s "$src/llvm-lipo"  "$dst/lipo"
    ln -s "$src/llvm-mc"    "$dst/as"
    ln -s "$src/llvm-nm"    "$dst/nm"
    ln -s "$src/llvm-strip" "$dst/strip"
    ln -s "$src/lld"        "$dst/ld"
fi

# build offload compiler
if [[ -v offload_target ]]; then

    # download newlib
    newlib="newlib-4.1.0"
    archive="$newlib.tar.gz"
    wget "ftp://sourceware.org/pub/newlib/$archive"
    tar -xaf "$archive"

    # prep gcc
    pushd "$gcc"
    ln -s ../$newlib/newlib newlib
    target=$(./config.guess)
    popd

    # build offload compiler
    mkdir build-offload
    pushd build-offload
    ../$gcc/configure --prefix="$prefix" \
        --disable-libquadmath \
        --disable-sjlj-exceptions \
        --enable-as-accelerator-for="$target" \
        --enable-languages=c,c++,fortran,lto \
        --enable-newlib-io-long-long \
        --target="$offload_target" \
        --with-build-time-tools="$prefix/$offload_target/bin" \
        --with-newlib
    make -j"$(nproc)"
    make install
    popd

    # remove link to newlib
    rm "$gcc/newlib"

    configure_opts+=(
        --enable-offload-targets="$offload_target"
    )
fi

# build host compiler
mkdir build-host
pushd build-host
../"$gcc"/configure "${configure_opts[@]}"
make -j"$(nproc)"
make install
