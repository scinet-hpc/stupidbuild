#!/bin/bash

prefix=$1
version=$2

# xlc/xlf require g++ in the path for GPU offloading
module load gcc/.core
gcc_root=$(dirname $(dirname $(which gcc)))

xl_root=/opt/ibm
if [[ ! -d $xl_root ]]; then
    read -rp "Path to XL $version root directory: " xl_root
    [[ -d $xl_root ]]
fi
xlc_root="$xl_root/xlC/16.1.1"
xlf_root="$xl_root/xlf/16.1.1"

# helper links for module file
ln -s "$xl_root" "$prefix/.xl"
ln -s "$xlc_root" "$prefix/.xlc"
ln -s "$xlf_root" "$prefix/.xlf"

mkdir "$prefix/etc"
xlc_cfg="$prefix/etc/xlc.cfg"
xlf_cfg="$prefix/etc/xlf.cfg"
"$xlc_root/bin/xlc_configure" -gcc "$gcc_root" -nosymlink -cuda "$MODULE_CUDA_PREFIX" -o "$xlc_cfg"
"$xlf_root/bin/xlf_configure" -gcc "$gcc_root" -nosymlink -cuda "$MODULE_CUDA_PREFIX" -o "$xlf_cfg"

mkdir "$prefix/bin"
pushd "$prefix/bin"

wrapper=".xlc-wrapper"
cat >"$wrapper" <<___
#!/bin/sh
PATH="\$PATH:$gcc_root/bin"
export LANG=en_US
export NLSPATH="$xl_root/msg/%L/%N:$xlc_root/msg/%L/%N"
exec "$xlc_root/bin/.orig/\$(basename "\$0")" -F "$xlc_cfg" "\${@}"
___
chmod a+x "$wrapper"
for name in xlc xlc++ xlC ; do
    for suffix in '' _r ; do
        ln -s "$wrapper" "$name$suffix"
    done
done

wrapper=".xlf-wrapper"
cat >"$wrapper" <<___
#!/bin/sh
PATH="\$PATH:$gcc_root/bin"
export LANG=en_US
export NLSPATH="$xl_root/msg/%L/%N:$xlf_root/msg/%L/%N"
exec "$xlf_root/bin/.orig/\$(basename "\$0")" -F "$xlf_cfg" "\${@}"
___
chmod a+x "$wrapper"
for name in xlf xlf90 xlf95 xlf2003 xlf2008 ; do
    for suffix in '' _r ; do
        ln -s "$wrapper" "$name$suffix"
    done
done

# march-native flag
cat >march-native <<___
#!/bin/sh
echo -qarch=auto
___
chmod a+x march-native

