#!/bin/bash

prefix=$1

[[ ! $CC =~ ^mpi ]]

# knem
knem=$(pkg-config --variable=prefix knem)

# slurm
if pkg-config slurm ; then
    slurm=$(pkg-config --variable=prefix slurm)
else
    slurm="/opt/slurm"
fi
[[ -d $slurm ]]

version="5.0.2"
archive="openmpi-${version}.tar.gz"
md5sum="7a07342c0cdec4525d1b51fcc92c682e"
url="https://www.open-mpi.org/software/ompi/v${version%.*}/downloads/$archive"

wget "$url"
md5sum --check <<<"$md5sum $archive"
tar -xaf "$archive"
cd "openmpi-$version"

# don't expose internal headers
# https://github.com/open-mpi/hwloc/issues/229
sed -i -e '/with_devel_headers=yes/d' contrib/platform/mellanox/optimized

configure_opts=(
    --enable-mpi1-compatibility
    --with-hwloc=internal
    --with-knem="$knem"
    --with-libevent=internal
    --with-platform=contrib/platform/mellanox/optimized
    --with-pmix=internal
    --with-prrte=internal
    --with-slurm="$slurm"
)

if module is-loaded cuda ; then
    configure_opts+=( --with-cuda="$MODULE_CUDA_PREFIX" )
fi

if module is-loaded ucx ; then
    configure_opts+=( --with-ucx="$MODULE_UCX_PREFIX" )
fi

./configure --prefix="$prefix" \
    "${configure_opts[@]}"

make all -j "$(nproc)"
make check
make install

