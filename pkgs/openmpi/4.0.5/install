#!/bin/bash

prefix=$1

[[ ! $CC =~ ^mpi ]]

# knem
knem=$(pkg-config --variable=prefix knem)

# slurm
if pkg-config slurm ; then
    slurm=$(pkg-config --variable=prefix slurm)
else
    slurm="/opt/slurm"
fi
[[ -d $slurm ]]

version="4.0.5"
archive="openmpi-${version}.tar.gz"
md5sum="9d54218acd56fe7327fcdb15dd40e620"
url="https://www.open-mpi.org/software/ompi/v${version%.*}/downloads/$archive"

wget "$url"
md5sum --check <<<"$md5sum $archive"
tar -xaf "$archive"
cd "openmpi-$version"

# don't expose internal headers
# https://github.com/open-mpi/hwloc/issues/229
sed -i -e '/with_devel_headers=yes/d' contrib/platform/mellanox/optimized

configure_opts=(
    --enable-mpi-cxx
    --enable-mpi1-compatibility
    --with-hwloc=internal
    --with-knem="$knem"
    --with-libevent=internal
    --with-platform=contrib/platform/mellanox/optimized
    --with-pmix=internal
    --with-slurm="$slurm"
)
if module is-loaded cuda ; then
    configure_opts+=( --with-cuda="$MODULE_CUDA_PREFIX" )
fi
if module is-loaded ucx ; then
    # --enable-mca-no-build=btl-uct # https://github.com/openucx/ucx/wiki/OpenMPI-and-OpenSHMEM-installation-with-UCX
    configure_opts+=( --with-ucx="$MODULE_UCX_PREFIX" )
else
    read -rp "HPCX directory: " hpcx
    [[ -d $hpcx ]]
    hcoll="$hpcx/hcoll"
    sharp="$hpcx/sharp"
    case $(uname -m) in
        x86_64)
            ucx="$hpcx/ucx"
            ;;
        ppc64le)
            ucx="/usr"
            ;;
    esac
    configure_opts+=(
        --with-hcoll="$hcoll"
        --with-ucx="$ucx"
    )
    LD_LIBRARY_PATH="$hcoll/lib:$sharp/lib:$LD_LIBRARY_PATH"
fi

./configure --prefix="$prefix" \
    "${configure_opts[@]}"

make all -j "$(nproc)"
make check
make install
