#!/bin/bash

#############################################################
## This script compiles OpenFOAM/22.06 on Niagara cluster. ## 
## ./stupid build ^gcc/11.3.0 ^openmpi/4.1.4+ucx-1.11.2 openfoam/22.06      ##
## B.Mundim | 2022.07.19                                   ## 
#############################################################

[[ $CC =~ ^mpi ]]

prefix="$1"
module load cmake/3.22.5
#module load boost/1.78.0 fftw/3.3.10 gmp/6.2.1 mpfr/4.1.0 cgal/5.4.2 
module load boost/1.78.0 fftw/3.3.10 gmp/6.2.1 mpfr/4.1.0

#which mpiicpc   >/dev/null && mpi=intelmpi
#which ompi_info >/dev/null && mpi=openmpi

### Set temporary environment as working directory 
export tmpdir=$PWD

### Set version
version=2212

### Download openfoam
pkg="OpenFOAM-v$version"
archive="$pkg.tgz"
url="https://dl.openfoam.com/source/v$version/$archive"
wget $url
tar -xaf $archive

### Download thirdparty
pkgTP="ThirdParty-v$version"
archive="$pkgTP.tgz"
url="https://dl.openfoam.com/source/v$version/$archive"
wget $url
tar -xaf $archive

### Change dir to OpenFOAM-v2206/
cd $pkg || exit


################################################################
## CHANGE OPENFOAM CONFIGURATION FILES FOR SYSTEMOPENMPI ETC. ##
################################################################

### Install OF in the temporary environment /dev/shm 

### Specify OF-v2206 temporary location first
sed -i 's,export WM_PROJECT_DIR="$projectDir",export WM_PROJECT_DIR='"${tmpdir}"'/'"${pkg}"',' etc/bashrc

### Use boost-system:
sed -i 's/boost_version=boost_1_74_0/boost_version=boost-system/' etc/config.sh/CGAL
sed -i 's,export BOOST_ARCH_PATH="$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$boost_version",export BOOST_ARCH_PATH='"${MODULE_BOOST_PREFIX}"',' etc/config.sh/CGAL

### Use cgal-system:
#sed -i 's/cgal_version=CGAL-4.14.3/cgal_version=cgal-system/' etc/config.sh/CGAL
#sed -i 's,export CGAL_ARCH_PATH="$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$cgal_version",export CGAL_ARCH_PATH='"${MODULE_CGAL_PREFIX}"',' etc/config.sh/CGAL

### Use system GMP if needed:
sed -i 's,# export GMP_ARCH_PATH=...,export GMP_ARCH_PATH='"${MODULE_GMP_PREFIX}"',' etc/config.sh/CGAL

### Use system MPFR if needed:
sed -i 's,# export MPFR_ARCH_PATH=...,export MPFR_ARCH_PATH='"${MODULE_MPFR_PREFIX}"',' etc/config.sh/CGAL

### Use system FFTW
sed -i 's/fftw_version=fftw-3.3.10/fftw_version=fftw-system/' etc/config.sh/FFTW
sed -i 's,export FFTW_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$fftw_version,export FFTW_ARCH_PATH='"${MODULE_FFTW_PREFIX}"',' etc/config.sh/FFTW


### Compiler settings (NOTE: OpenFOAM still doesn't vectorize well)
# The following changes might have marginal speed up only:
sed -i 's/cARCH       = -m64/cARCH       = -m64 -march=native/' wmake/rules/linux64Gcc/c
sed -i 's/c++ARCH     = -m64 -pthread/c++ARCH     = -m64 -march=native -pthread/' wmake/rules/linux64Gcc/c++

### Ignore warnings - Source OF environment to initiate installation
export FOAM_VERBOSE=true
set +eu
  . etc/bashrc


############################
## RUN OPENFOAM INSTALLER ##
############################

### Run OF installer
./Allwmake -j "$(nproc)" -q  2>&1 | tee log.Allwmake


#################################################
## COPY INSTALLED OPENFOAM TO PREFIX DIRECTORY ##
#################################################

### Now copy entire OF directory to the prefix=/scinet/niagara/opt/... 
# with proper permissions
cd "$tmpdir" || exit
chmod -R u+rw $pkg $pkgTP
cp -r $pkg $pkgTP "$prefix"/

### Change WM_PROJECT_DIR to MODULE_OPENFOAM_PREFIX to make it work 
# on the cluster when it is loaded as a module
sed -i 's,'"$tmpdir"','"$prefix"',g' "$prefix"/$pkg/etc/bashrc 

