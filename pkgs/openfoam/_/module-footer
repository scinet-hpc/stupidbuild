# Remove "." character from version string
set VER [string map { "." "" } $version]

prepend-path PATH ${prefix}/OpenFOAM-v${VER}/wmake
prepend-path PATH ${prefix}/OpenFOAM-v${VER}/bin
prepend-path PATH ${prefix}/OpenFOAM-v${VER}/platforms/linux64GccDPInt32Opt/bin
prepend-path PATH ${prefix}/OpenFOAM-v${VER}/site/${VER}/platforms/linux64GccDPInt32Opt/bin
prepend-path PATH $::env(HOME)/OpenFOAM/$::env(USER)-v${VER}/platforms/linux64GccDPInt32Opt/bin
prepend-path PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64Gcc/ADIOS2-2.7.1/bin

prepend-path LD_LIBRARY_PATH ${prefix}/OpenFOAM-v${VER}/platforms/linux64GccDPInt32Opt/lib/dummy
prepend-path LD_LIBRARY_PATH $::env(MODULE_OPENMPI_PREFIX)/lib
prepend-path LD_LIBRARY_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64Gcc/ADIOS2-2.7.1/lib64
#prepend-path LD_LIBRARY_PATH $::env(MODULE_BOOST_PREFIX)/lib
#prepend-path LD_LIBRARY_PATH $::env(MODULE_GMP_PREFIX)/lib
#prepend-path LD_LIBRARY_PATH $::env(MODULE_MPFR_PREFIX)/lib
#prepend-path LD_LIBRARY_PATH $::env(MODULE_FFTW_PREFIX)/lib
prepend-path LD_LIBRARY_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64GccDPInt32/lib
prepend-path LD_LIBRARY_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64GccDPInt32/lib/sys-openmpi
prepend-path LD_LIBRARY_PATH ${prefix}/OpenFOAM-v${VER}/platforms/linux64GccDPInt32Opt/lib
prepend-path LD_LIBRARY_PATH ${prefix}/OpenFOAM-v${VER}/platforms/linux64GccDPInt32Opt/lib/sys-openmpi
prepend-path LD_LIBRARY_PATH ${prefix}/OpenFOAM-v${VER}/site/${VER}/platforms/linux64GccDPInt32Opt/lib
prepend-path LD_LIBRARY_PATH $::env(HOME)/OpenFOAM/$::env(USER)-v${VER}/platforms/linux64GccDPInt32Opt/lib


#prepend-path LIBRARY_PATH ${prefix}/OpenFOAM-v${VER}/platforms/linux64GccDPInt32Opt/lib
#prepend-path LIBRARY_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64GccDPInt32/lib
#prepend-path LIBRARY_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64GccDPInt32/lib/openmpi-system


### Application library and binary main directories
setenv FOAM_APPBIN ${prefix}/OpenFOAM-v${VER}/platforms/linux64GccDPInt32Opt/bin
setenv FOAM_LIBBIN ${prefix}/OpenFOAM-v${VER}/platforms/linux64GccDPInt32Opt/lib
setenv FOAM_SITE_APPBIN ${prefix}/OpenFOAM-v${VER}/site/${VER}/platforms/linux64GccDPInt32Opt/bin
setenv FOAM_SITE_LIBBIN ${prefix}/OpenFOAM-v${VER}/site/${VER}/platforms/linux64GccDPInt32Opt/lib
setenv FOAM_USER_APPBIN $::env(HOME)/OpenFOAM/$::env(USER)-v${VER}/platforms/linux64GccDPInt32Opt/bin
setenv FOAM_USER_LIBBIN $::env(HOME)/OpenFOAM/$::env(USER)-v${VER}/platforms/linux64GccDPInt32Opt/lib

setenv FOAM_EXT_LIBBIN ${prefix}/ThirdParty-v${VER}/platforms/linux64GccDPInt32/lib

### Convenience environment variables
setenv FOAM_API ${VER}
setenv FOAM_ETC ${prefix}/OpenFOAM-v${VER}/etc
setenv FOAM_APP ${prefix}/OpenFOAM-v${VER}/applications
setenv FOAM_SRC ${prefix}/OpenFOAM-v${VER}/src
setenv FOAM_TUTORIALS ${prefix}/OpenFOAM-v${VER}/tutorials
setenv FOAM_UTILITIES ${prefix}/OpenFOAM-v${VER}/applications/utilities
setenv FOAM_SOLversion//.S ${prefix}/OpenFOAM-v${VER}/applications/solvers
setenv FOAM_RUN $::env(SCRATCH)/OpenFOAM/$::env(USER)-v${VER}/run


### wmake environment variables
setenv WM_PROJECT OpenFOAM
setenv WM_PROJECT_version//.SION v${VER}
setenv WM_PROJECT_DIR ${prefix}/OpenFOAM-v${VER}
setenv WM_PROJECT_USER_DIR $::env(HOME)/OpenFOAM/$::env(USER)-v${VER}
setenv WM_THIRD_PARTY_DIR ${prefix}/ThirdParty-v${VER}

setenv WM_ARCH linux64
setenv WM_COMPILER Gcc
setenv WM_COMPILE_OPTION Opt
setenv WM_COMPILER_LIB_ARCH 64
setenv WM_COMPILER_TYPE system
setenv WM_DIR ${prefix}/OpenFOAM-v${VER}/wmake
setenv WM_LABEL_OPTION Int32
setenv WM_LABEL_SIZE 32 
setenv WM_PRECISION_OPTION DP
setenv WM_OPTIONS linux64GccDPInt32Opt

### ThirdParty environment variables
#setenv BOOST_ARCH_PATH $::env(MODULE_BOOST_PREFIX)
#setenv FFTW_ARCH_PATH $::env(MODULE_FFTW_PREFIX)
setenv CGAL_ARCH_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64Gcc/CGAL-4.14.3
setenv SCOTCH_ARCH_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64GccDPInt32/scotch_6.1.0
setenv ADIOS2_ARCH_PATH ${prefix}/ThirdParty-v${VER}/platforms/linux64Gcc/ADIOS2-2.7.1

setenv FOAM_MPI sys-openmpi
setenv WM_MPLIB SYSTEMOPENMPI
setenv MPI_ARCH_PATH $::env(MODULE_OPENMPI_PREFIX)

### Legacy and extra environment variables
setenv MPI_BUFFER_SIZE 20000000
#setenv FOAMY_HEX_MESH yes
setenv FOAM_INST_DIR ${prefix}/OpenFOAM-v${VER}
setenv WM_PROJECT_INST_DIR ${prefix}/OpenFOAM-v${VER}/
setenv WM_ARCH_OPTION 64
setenv WM_LINK_LANGUAGE c++
setenv WM_OSTYPE POSIX

setenv WM_CC mpicc
setenv WM_CX mpicxx
setenv WM_CXXFLAGS -m64 -march=native -fPIC -std=c++11
setenv WM_CFLAGS -m64 -march=native -fPIC 
setenv WM_LDFLAGS -m64 -march=native 
###
