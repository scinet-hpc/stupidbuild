# Miscellaneous scripts

* `scrape-help-from-spack`: Creates missing `module-help` and `module-whatis`
files, using the [spack](https://github.com/spack/spack) git repository.

