# stupidbuild

A stupid HPC software management tool.

Package installation scripts are written in bash, and self-contained.
stupidbuild handles the bookkeeping of organizing modules in a hierarchy,
and in most cases can automatically create modules.

## Getting started

```
export STUPID_ROOT=/path/to/root
./stupid bootstrap
```

This requires a C++ compiler capable of building gcc.
Afterwards, GCC and [LMOD](https://github.com/TACC/Lmod) will be installed in `$STUPID_ROOT`.
The GCC programs are all prefixed with `core-`, e.g., `core-g++`.
Source `$STUPID_ROOT/profile.sh` to modify your shell environment.

## Building packages

### Example 1

```
./stupid build gcc/8.4.0
```
This installs GCC version 8.4.0 in `$STUPID_ROOT/opt/base/gcc/8.4.0/`,
and creates a `$STUPID_ROOT/modules/base/gcc/8.4.0` module.

You can try out the new module using the `stupid shell` command:
```
$ ./stupid shell
stupid shell> module load gcc/8.4.0
stupid shell> gcc -dumpversion
8.4.0
```

### Example 2

```
./stupid build ^gcc/8.4.0 openmpi/4.0.4
```
This installs Open MPI version 4.0.4 in `$STUPID_ROOT/opt/gcc-8.4.0/openmpi/4.0.4/`,
and creates a `$STUPID_ROOT/modules/gcc-8.4.0/openmpi/4.0.4` module.
The `^` indicates that `gcc/8.4.0` is the parent module of `openmpi/4.0.4` in the hierarchy.

### Example 3

```
./stupid build ^gcc/8.4.0 ^openmpi/4.0.4 petsc/3.13.3 +mkl/2020u1
```
This installs PETSc version 3.13.3 in
`$STUPID_ROOT/opt/gcc-8.4.0-openmpi-4.0.4/petsc/3.13.3+mkl-2020u1/`,
and creates a `$STUPID_ROOT/modules/gcc-8.4.0-openmpi-4.0.4/petsc/3.13.3+mkl-2020u1` module.
The `+` indicates an that `mkl/2020u1` is an optional prerequisite.

## Writing packages

What does `stupid build gcc/8.4.0` actually do?
First, it looks for the following bash scripts:

```
pkgs/
  gcc/
    8.4.0/
      install
      module
      test
```

Then it runs each script in a clean environment and temporary working directory,
with the bash flags `-efux -o pipefail` enabled.
Two arguments are passed: the installation directory, and the module version.

Either `install` or `module` must exist.

If `module` exists, its output is used to create the module file.
Otherwise, `stupid build` will create the module automatically by inspecting the install directory.
For example, if `<install-dir>/bin` exists, it is prepended to `PATH`, etc.

The newly created module is loaded before running `test`.

In addition to the above scripts, `stupid build` also looks for the following files:

* `module-footer`: extra content to append to the module file.
* `module-has-children`: if this exists, the module is a parent and modifies `MODULEPATH`.
* `module-help`: the help message for the module.
* `module-whatis`: each line of this file is a separate module `whatis` entry.

For more complex builds, e.g., `stupid build ^gcc/8.4.0 arpack-ng/3.9.0 +mkl/2020u1`,
the parents and prerequisites, `gcc/8.4.0` and `mkl/2020u1` in this case,
will be loaded before running the scripts.
For parent `^` modules, `module-has-children` must exist,
while for prerequisite `+` modules, `module-has-children` must *not* exist.

For convienence, you can share files between versions by putting them `pkgs/<name>/_/`.

## Usage

```
$ ./stupid --help
stupidbuild: a stupid HPC software management tool

Usage: stupid [COMMAND] [ARG]...

Commands:
  bootstrap     Initialize.
  build         Build package.
  deploy        Deploy package into production.
  help          Show usage.
  installed     List installed packages.
  modulefile    Show package module filename.
  prefix        Show package installation directory.
  recent        List recently installed packages.
  remove        Remove partially installed package.
  root          Show root directory.
  shell         Start shell environment.
  shellcheck    Run shellcheck on package scripts.
  verify        Verify package integrity.

```
