#!/bin/bash
# shellcheck disable=SC2251
set -efu -o pipefail
trap 'echo $0 failed at line $LINENO' ERR

module purge

now() { date +%s; }
start_time=$(now)

STUPID_ROOT=$(mktemp -d)
export STUPID_ROOT STUPID_CONFIRM=false

cleanup() {
    chmod -R +w "$STUPID_ROOT"
    rm -r "$STUPID_ROOT"
    echo "test took $(( ( $(now) - start_time ) / 60 )) minutes"
}
trap cleanup EXIT

# find intel
intel_version=$(module --quiet load intel && module --terse list intel |& gawk -F/ '{print $2}')
intel_license=$(module --quiet load intel && sh -c 'echo $INTEL_LICENSE_FILE')
intel_root=$(module --quiet load intel && realpath "$(dirname "$(which icc)")/../../../..")
[[ -d $intel_root/mkl ]]

cmake="cmake/3.17.3"
gcc="gcc/8.4.0"
intel="intel/$intel_version"
intelmpi="intelmpi/$intel_version"
mkl="mkl/$intel_version"
openmpi="openmpi/4.0.4"
petsc="petsc/3.13.3"

# bootstrap
(
    module load gcc/.core
    ./stupid bootstrap
)

# non-existent packages
! ./stupid build doesnt/exist
! ./stupid build ^doesnt/exist doesnt/exist
! ./stupid build +doesnt/exist doesnt/exist

# basics
./stupid build gcc/.core
! ./stupid build gcc/.core
./stupid build "$cmake"

# intel modules
./stupid build "$intel" <<___
$intel_root
$intel_license
___
! ./stupid build "$intelmpi"
! ./stupid build "+$intel" "$intelmpi"
./stupid build "^$intel" "$intelmpi"

# gcc
./stupid build "$gcc"

# gcc + intelmpi/mkl
for module in "$intelmpi" "$mkl"; do
    ! ./stupid build "$module"
    ! ./stupid build "^$intel" "$module"
    ! ./stupid build "^$intel" "^$intelmpi" "$module"
    ! ./stupid build "+$gcc" "$module"
    ./stupid build "^$gcc" "$module"
done

# openmpi
! ./stupid build "$gcc" "$openmpi"
! ./stupid build "+$gcc" "$openmpi"
./stupid build "^$gcc" "$openmpi"
./stupid build "^$intel" "$openmpi"

# petsc
./stupid build "^$gcc" "^$openmpi" "$petsc" "+$mkl"
./stupid build "^$intel" "^$intelmpi" "$petsc"

echo
echo "$(tput bold)$(tput setaf 2)success!$(tput sgr0)"
