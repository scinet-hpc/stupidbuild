`stupid build` runs scripts in a clean, minimal environment.
This directory allows you to customize that environment:

* `bin` is added to `PATH`.

